/**
 * GEII's APP TP3 Exo 5 Detailled Correction
 * Copyright (C) 2018 Sam Vie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * contact@sakiut.fr
 */

/* Je pars ici du principe que les précédentes corrections ont été lues, si quelque chose n'est pas expliqué, allez y
 * jeter un oeil.
 */

// On ajoute 3 librairies : stdlib et time pour l'aléatoire, et conio pour le getch
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>

// On déclare une variable globale entière Choice (int)
int Choice;

// On déclare deux fonctions, une void (qui ne retournera rien, et une int qui retournera donc un... entier !
void Menu();
int Ask();

int main()
{
    // On définit ici trois entiers (int) en local
    int Replay, RandomNumber, i;

    do // Début d'un do {...} while (condition)
    {
        i = 1; // On met i (notre compteur d'essais) à 1 pour le premier essai
        Choice = 101; /* On choisit Choice à 101 pour qu'il soit impossible que le nombre aléatoire tombe dessus
                       * (on pourrait très bien mettre n'importe quelle valeur non comprise entre 0 et 99 inclus)
                       */

        /* Je fais ici un apparté pour expliquer le fonctionnement de la fonction rand : il s'agit d'une gigantesque
         * liste de nombres qui n'a aucun sens. le problème, c'est que si on l'utilise telle quelle, les mêmes chiffres
         * ressortiront à chaque essai.
         *
         * Pour éviter cela, il faut initialiser la liste à un nombre qui change (c'est le rôle de srand()), mais
         * celui-ci devrait être aléatoire, donc le serpent se mort la queue... sauf qu'une idée simple a été trouvée :
         * On initialise srand avec time(NULL) qui représente le nombre de secondes écoulées depuis le 1er janvier 1980.
         * Étant donné qu'on ne peut lancer le programme deux fois à la même valeur, on peut considérer rand comme étant
         * aléatoire.
         *
         * Si pas compris, plus d'infos ici :
         * https://openclassrooms.com/fr/courses/1067141-laleatoire-en-c-et-c-se-servir-de-rand/
         */

        printf("\nA number between 0 and 99 (included) has been chosen.");
        srand(time(NULL));
        RandomNumber = rand()%100; // On choisit le nombre aléatoire (en le limitant à 100 avec %100)
        while(Choice != RandomNumber) // Tant que le choix de l'utilisateur est différent du nombre aléatoire
        {
            Menu(); // On affiche le Menu
            if (Choice == RandomNumber) // Si le choix est identique au nombre aléatoire
            {
                break; // On force l'arrêt de la boucle
            }
            if (Choice > RandomNumber) // Si le choix est supérieur à l'aléatoire
            {
                printf("\nTrop haut"); // On est trop haut
            }
            else                       // Sinon
            {
                printf("\nTrop bas");  // On est trop bas
            }
            i += 1; // On incrémente i (<=> i++ <=> i = i + 1)
        }
        // Quand la condition n'est plus vraie, on sort de la boucle, c'est donc forcément qu'on a gagné
        printf("\nYou win!");
        printf("\nScore : %d", i); // On affiche le score qui correspond au nombre d'essais
        Replay = Ask(); // On met Replay à la valeur retournée par la fonction Ask (voir plus bas)
    }
    while (Replay == 1); // Tant que Replay == 1, c'est-à-dire tant que l'utilisateur veut rejouer.
    return 0; // Fin de la boucle → Fin du programme
}

void  Menu()
{
    // Fonction qui récupère le choix de l'utilisateur et le stocke dans la variable globale Choice

    printf("\nMake your choice: ");
    scanf("%d", &Choice);
}

int Ask()
{
    // On définit la variable de type caractère Question (char)
    char Question;

    while (1) // Tant que 1 (Pour toujours)
    {
        printf("\nWould you like to retry? (Y/N) "); // Voulez-vous recommencer ?
        Question = (char) getche(); // On récupère l'entrée
        switch (Question) // Selon Question
        {
            // Si Question = 'Y' ou 'y'
            case 'Y':
            case 'y':
                return 1; // On renvoie 1: l'utilisateur veut rejouer - cela met fin à la fonction et à la boucle

            // Si Question = 'N' ou 'n'
            case 'N':
            case 'n':
                return 0; // On renvoie 0: l'utilisateur ne veut pas rejouer - cela met fin à la fonction et à la boucle

            // Sinon
            default:
                printf("\nPlease type Y or N"); // L'utilisateur n'a pas écrit ni 'N' ni 'n' ni 'Y' ni 'y' : Erreur
        } // Si erreur, on revient du même coup obligatoirement au début.
    }
}
