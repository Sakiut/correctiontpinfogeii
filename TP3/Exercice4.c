/**
 * GEII's APP TP3 Exo 4 Detailled Correction
 * Copyright (C) 2018 Sam Vie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * contact@sakiut.fr
 */

/* Je pars ici du principe que les précédentes corrections ont été lues, si quelque chose n'est pas expliqué, allez y
 * jeter un oeil.
 */

// Ce programme étant une suite directe des précédents, je ne commenterai que les ajouts

#include <stdio.h>

float Note[10], Somme, Moyenne;
int i, Tours;

int main()
{
    printf("Saisir le nombre de notes à entrer : ");
    scanf("%d", &Tours);
    Somme = 0;
    for (i = 0; i < Tours; i++)
    {
        do // On ajoute une boucle do {...} while (condition), je détaille le pourquoi du comment plus bas
        {
            printf("\nSaisir note #%d ", i + 1);
            scanf("%f", &Note[i]);
            if (Note[i] > 20 || Note[i] < 0) // Si la note n'est pas comprise entre 0 et 20
            {
                printf("Valeur non comprise entre 0 et 20"); // On le signale
            }
            else
            {
                Somme = Somme + Note[i]; // Sinon on incrémente normalement la somme
            }
        }
        while (Note[i] > 20 || Note[i] < 0); // Tant que la note n'est pas comprise entre 0 et 20

        /* Cette boucle while sert à faire en sorte que si l'utilisateur saisit une valeur fausse, on lui propose de
         * saisir à nouveau la même et pas la suivante. En gros, il "bloque" la boucle for tant que la valeur saisie
         * n'est pas juste.
         */
    }
    Moyenne = Somme / Tours;
    printf("La moyenne est : %.2f", Moyenne);
    return 0;
}
