/**
 * GEII's APP TP3 Exo 2 Detailled Correction
 * Copyright (C) 2018 Sam Vie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * contact@sakiut.fr
 */

/* Je pars ici du principe que les précédentes corrections ont été lues, si quelque chose n'est pas expliqué, allez y
 * jeter un oeil.
 */

// Une fois de plus, pas besoin de plus que stdio.h
#include <stdio.h>

/* On définit 4 variables : deux réels (float) Somme et Moyenne, un entier (int) i et un tableau (ou une table) de float
 * Note de dimension 10, ce qui signifie que l'on aura une sorte de tableau à 10 cases dans lequel on pourra mettre une
 * valeur par case de type float, donc 10 valeurs en tout.
 *
 * On pourrait tout aussi bien les déclarer en local, simple choix de clarté pour alléger la fonction main.
 */
float Note[10], Somme, Moyenne;
int i;

int main()
{
    Somme = 0; // On définit la somme à 0

    /* On lance ensuite une boucle POUR ... (for (...)) qui prend 3 arguments :
     *
     *      - Argument 1 : i = 0  : on utilisera la variable i et on commencera à 0
     *      - Argument 2 : i < 10 : on s'arrêtera à la dernière valeur de i strictement inférieure à 10
     *      - Argument 3 : i++    : i augmentera de 1 à chaque tour
     *                              À noter qu'on pourrait aussi écrire i = i + 1 ou i += 1
     *
     * En français on pourrait dire « Pour i allant de 0 à 9 avec un pas de 1 »
     */
    for (i = 0; i < 10; i++)
    {
        printf("\nSaisir note #%d ", i + 1); /* On demande de saisir la note. (i va de 0 à 9 donc on ajoute 1 pour
                                              * que l'affichage ne choque pas.
                                              */
        scanf("%f", &Note[i]); // On insère l'entrée dans la "case" de la table Note correspondante
        Somme = Somme + Note[i]; // On ajoute à la somme la note que l'utilisateur vient de saisir
    }
    Moyenne = Somme / 10; // On divise la somme des notes par 10, le nombre de notes, pour obtenir la moyenne
    printf("La moyenne est : %.2f", Moyenne); // On affiche la moyenne
    return 0; // Fin du programme, tout va bien.
}
