/**
 * GEII's APP TP3 Exo 3 Detailled Correction
 * Copyright (C) 2018 Sam Vie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * contact@sakiut.fr
 */

/* Je pars ici du principe que les précédentes corrections ont été lues, si quelque chose n'est pas expliqué, allez y
 * jeter un oeil.
 */

// Ce programme étant une suite directe du précédent, je ne commenterai que les ajouts

#include <stdio.h>

// On ajoute une variable entière (int) Tours

float Note[10], Somme, Moyenne;
int i, Tours;

int main()
{
    // On demande le nombre de notes qu'il faudra entrer.

    printf("Saisir le nombre de notes à entrer : ");
    scanf("%d", &Tours);

    Somme = 0;
    for (i = 0; i < Tours; i++) // On remplace le 10 de i < 10 par Tours, le nombre de notes à saisir.
    {
        printf("\nSaisir note #%d ", i + 1);
        scanf("%f", &Note[i]);
        Somme = Somme + Note[i];
    }
    Moyenne = Somme / Tours; // On remplace le / 10 par la variable Tours qui représente le nombre de notes saisies
    printf("La moyenne est : %.2f", Moyenne);
    return 0;
}
