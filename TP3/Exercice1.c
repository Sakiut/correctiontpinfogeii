/**
 * GEII's APP TP3 Exo 1 Detailled Correction
 * Copyright (C) 2018 Sam Vie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * contact@sakiut.fr
 */

/* Je pars ici du principe que les précédentes corrections ont été lues, si quelque chose n'est pas expliqué, allez y
 * jeter un oeil.
 */

// On importe stdio, pas besoin de plus
#include <stdio.h>

/* On définit un seule variable globale Ans de type entier (int) : elle pourra donc être utilisée dans toutes les
 * fonctions du programme.
 */
int Ans;

// On définit une fonction void (qui ne retournera pas de valeur) Menu qui affichera notre menu
void Menu();

int main()
{
    /* On crée trois variables locales float (réelles) B, H et V qui couvriront tous les cas de valeurs de l'exercice.
     * On les crée en local car elles ne seront pas utilisées dans les autres fonctions.
     */
    float B, H, V;

    // On lance une structure FAIRE ... TANT QUE condition <=> do { ... } while (condition)
    do
    {
        Menu(); // On affiche le menu via la fonction Menu (voir plus bas)

        /* On utilise ci-dessous un switch, c'est-à-dire que l'on va regarder la valeur de la variable passée en
         * paramètre (ici Ans) et envisager plusieurs cas de figure :
         */
        switch (Ans)
        {
            case 1 : // Si Ans vaut 1

                printf("Calcul Volume Pyramide"); // L'utilisateur a choisi le calcul de la Pyramide

                // On récupère les valeurs nécessaires au calcul (entrées par l'utilisateur)
                printf("\nSaisir la dimension de la Base B (en m2) : ");
                scanf("%f", &B);
                printf("\nSaisir la dimension de la Hauteur H (en m) : ");
                scanf("%f", &H);

                // On calcule
                V = (float) (1.0/3) * B * H;

                // On affiche le résultat
                printf("\nVolume de la Pyramide : %.2f m3\n", V);

                /* Et on met fin au case par un break qui permet de sortir du switch
                 * ATTENTION : S'il est oublié, le case suivant sera également exécuté et tout ce qu'il y aura avant le
                 * break suivant ou la fin du switch.
                 */
                break;
            case 2 : // Si Ans vaut 2, même chose que ci-dessus pour le Cylindre Oblique
                printf("Calcul Volume Cylindre Oblique");
                printf("\nSaisir la dimension de la Base B (en m2) : ");
                scanf("%f", &B);
                printf("\nSaisir la dimension de la Hauteur H (en m) : ");
                scanf("%f", &H);
                V = (float) B * H;
                printf("\nVolume du Cylindre Oblique : %.2f m3\n", V);
                break;
            case 3 : // Si Ans vaut 3, même chose que ci-dessus pour le Cone Oblique
                printf("Calcul Volume Cone Oblique");
                printf("\nSaisir la dimension de la Base B (en m2) : ");
                scanf("%f", &B);
                printf("\nSaisir la dimension de la Hauteur H (en m) : ");
                scanf("%f", &H);
                V = (float) (1.0/3) * B * H;
                printf("\nVolume du Cone Oblique : %.2f m3\n", V);
                break;
            case 4 : // Si Ans vaut 4, on écrit « Au revoir », la sortie de la boucle se fera plus bas
                printf("\nAu revoir !");
                break;
            default: // Sinon, si aucun des cas n'est rempli par défaut, on a une erreur.
                printf("Erreur de saisie - Je n'ai pas compris !");
                printf("\nRecommencez !\n");
                break;
        }
    }
    while (Ans != 4); // Tout cela tant que Ans est différent de 4
    /* Ainsi, si l'utilisateur saisit 4, cette condition devient fausse (elle passe à 0), le programme met automatique-
     * ment fin à la boucle while.
     */
    return 0; // Fin d'exécution, tout s'est bien passé !
}

void Menu()
{
    // On affiche le menu
    printf("Calculer volume :\n\n\t1> Pyramide\n\t2> Cylindre oblique\n\t3> Cone oblique\n\t4> Quitter\n\n");
    scanf("%d", &Ans); // On récupère la valeur que l'on place dans la variable globale Ans.

    // On ne retourne rien car cette fonction est un void.
}

// On a dépassé ici les 100 lignes de code, bien joué ! o7
