/**
 * GEII's APP TP2 Exo 1 Detailled Correction
 * Copyright (C) 2018 Sam Vie
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * contact@sakiut.fr
 */

// Import de toutes les fonctions nécessaires au fonctionnement de la console (printf, scanf...)
#include <stdio.h>

/* Rappel : ceci est un commentaire, il est ignoré par le programme
 *
 * Normalement on doit mettre les variables en anglais, je fais une exception ici.
 * En C, on est également censés écrire les mots collés différentiés par des majuscules
 * Exemple : ne pas nommer une variable "produit_multiplication" mais plutôt "ProduitMultiplication".
 *
 * Définition des variables : 5 variables réelles (ou nombres à virgule flottante, float).
 *
 * Je les définis ici en mode global (c'est-à-dire que si on mettait une autre fonction, elle pourrait également bénéficier
 * de ces variables. On pourrait donc tout aussi bien les définir au sein de la fonction main().
 */

float Moyenne, Note1, Note2, Note3, Note4;

int main ()
{
    printf("Entrer note #1 : "); // Première instruction : on écrit simplement le texte dans la console
    scanf("%f", &Note1);         /* On récupère ici l'entrée de l'utilisateur. Le "%f" définit le type d'entrée, ici
                                  * float, se référer au cours pour plus d'infos concernant les codes des autres formats
                                  *
                                  * &Note1 représente ici la variable où va arriver l'entrée, précédée d'un & (ne pas
                                  * l'oublier sous peine d'erreur !)
                                  */
    printf("Entrer note #2 : "); // Même chose ici pour la seconde note
    scanf("%f", &Note2);

    printf("Entrer note #3 : "); // Même chose ici pour la troisième note
    scanf("%f", &Note3);

    printf("Entrer note #4 : "); // Même chose ici pour la quatrième note
    scanf("%f", &Note4);

    Moyenne = (Note1 + Note2 + Note3 + Note4) / 4; // On fait la moyenne des notes

    printf("\nMoyenne des 4 notes : %.2f", Moyenne); // On affiche la moyenne à l'écran
    /* Ici, on note %.2f l'emplacement où sera affiché le contenu de la variable Moyenne. Une fois de plus, le f définit
     * le type de variable (float ici de nouveau), le .2 introduit par contre le fait que si la Moyenne a plus de deux
     * décimales, elle n'affichera que les deux premières (Attention /!\ C n'arrondira pas,il se contentera de tronquer
     * (couper)).
     * Le \n symbolise un retour à la ligne.
     */

    return 0; /* On retourne 0, ce qui signifie d'une part que le programme est fini et d'autre part que tout s'est bien
               * passé (on pourrait retourner une autre valeur, mais C considèrerait qu'une erreur s'est produite).
               */
}