/**
 * GEII's APP TP2 Exo 3 Detailled Correction
 * Copyright (C) 2018 Sam Vie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * contact@sakiut.fr
 */

/* Je pars ici du principe que les précédentes corrections ont été lues, si quelque chose n'est pas expliqué, allez y
 * jeter un oeil.
 */

#include <stdio.h>

/* À partir de maintenant, je me conformerai aux conventions de code en C, les noms de variables seront donc en anglais.
 *
 * On définit deux variables de type float, dont un tableau : NotesTable, qui a une dimension de 3 (entre crochets)
 * Un tableau de dimension n, aussi appelée une table, peut contenir n valeurs de son type. Ici, NotesTable peut donc
 * contenir jusqu'à trois float.
 */

float NotesTable[3], Average;

int main ()
{
    printf("Entrer note du QC (sur /10) : "); // On initialise la console côté utilisateur.
    scanf("%f", &NotesTable[0]); /* On récupère l'entrée de l'utilisateur (la note donc), et on la place dans le premier
                                  * emplacement de la table NotesTable, qui est numérotée 0.
                                  */

    // On récupère la seconde note de la même manière
    printf("Entrer note du DS (sur /20) : ");
    scanf("%f", &NotesTable[1]);

    // On récupère la troisième note de la même manière
    printf("Entrer note du TP (sur /20) : ");
    scanf("%f", &NotesTable[2]);

    Average = (float) (2 * NotesTable[0] + NotesTable[2] + 4 * NotesTable[1]) / 6;
    /* On fait ici la moyenne des notes suivant le calcul donné dans le sujet (je crois que le prof s'est foiré sur les
     * coefficients mais chut il faut rien dire :) du coup on laisse comme ça).
     */

    printf("Moyenne est : %.2f", Average); // On affiche simplement la moyenne, déjà vu dans les correcs précédentes.
    
    return 0; // On retourne 0, le programme est fini, tout s'est bien passé.
}