/**
 * GEII's APP TP2 Exo 2 Detailled Correction
 * Copyright (C) 2018 Sam Vie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * contact@sakiut.fr
 */

/* Je pars ici du principe que les précédentes corrections ont été lues, si quelque chose n'est pas expliqué, allez y
 * jeter un oeil.
 */

#include <stdio.h>
// On ajoute la librairie conio.h pour bénéficier du "getche ()".
#include <conio.h>

/* On définit une seule variable de type char. À partir de maintenant, je me conformerai aux conventions de code en C,
 * les noms de variables seront donc en anglais.
 */

char Character;

int main ()
{
    printf("Entrer un char : "); // On initialise la console côté utilisateur.
    Character = (char) getche(); /* On récupère un char à l'aide de getch() (on ajoute un e pour "echo" qui fait en
                                  * sorte que la console affiche le caractère que l'utilisateur tape. Comme getch() peut
                                  * également être utilisé pour récupérer un entier, on force le char en mettant (char)
                                  * devant l'appel de la fonction.
                                  */
    Character += 1; /* On incrémente la valeur de Character d'un, c'est-à-dire qu'on le force à passer à la
                     * lettre suivante dans l'alphabet. (Pout tout a, l'expression "a += 1" est un diminutif de
                     * "a = a + 1").
                     */
    printf("\nChar modifié : %c", Character); /* On retoune le contenu de Character en précisant son type, char avec
                                               * "%c".
                                               * On n'oublie pas le \n pour le retour à la ligne 
                                               */
    return 0; // On retourne 0, le programme est fini, tout s'est bien passé.
}
