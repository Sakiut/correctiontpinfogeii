/**
 * GEII's APP TP2 Exo 3 Detailled Correction
 * Copyright (C) 2018 Sam Vie
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * contact@sakiut.fr
 */

/* Je pars ici du principe que les précédentes corrections ont été lues, si quelque chose n'est pas expliqué, allez y
 * jeter un oeil.
 */

#include <stdio.h>
#include <conio.h>

// On définit la constante Pi
#define Pi 3.141592

/* À partir de maintenant, je me conformerai aux conventions de code en C, les noms de variables seront donc en anglais.
 * (Les print aussi parce que j'ai la flemme de traduire '-')
 *
 * On définit quatre variables : 3 float qui désignent le Rayon, le Diamètre et le Volume de la sphère que l'on cherche
 * à calculer, plus un char qui nous servira à définir quel calcul sera exécuté. Ici, il est impératif que les variables
 * soient déclarées en global étant donné que nous en aurons besoin dans différentes fonctions
 */

float Radius, Diameter, Volume;
char Type;

// On définit également trois fonctions "void" (qui ne retourneront rien) qui serviront à gérer les affichages.

void DisplayRadius();
void DisplayDiameter();
void DisplayError();

int main ()
{
    printf("Diameter or radius? (meters) (D/R)"); // On initialise la console côté utilisateur.
    Type = (char) getche(); // On récupère le type (diamètre ou radius)

    if (Type == 'R' || Type == 'r') // Si l'utilisateur entre la lettre 'R' ou 'r' (oui c'est pas pareil)
    {
        DisplayRadius(); // Direction plus bas pour voir ce que fait cette fonction !
        Volume = (float) ((4.0 / 3) * Pi * (Radius * Radius * Radius));
        /* On calcule ci-dessus le volume de la sphère avec un rayon. On met un (float) entre parenthèses devant
         * l'expression pour être sûr qu'elle retourne un float, et on ajoute par mesure de précaution un ".0" derrière
         * le 1 qui est au numérateur.
         */
    }
    else // Sinon
    {
        if (Type == 'D' || Type == 'd') // Si l'utilisateur a saisit 'D' ou 'd'
        {
            DisplayDiameter(); // Direction plus bas !
            Volume = (float) ((1.0 / 6) * Pi * (Diameter * Diameter * Diameter)); // On fait le calcul du volume
        }
        else // Sinon (donc si l'utilisateur n'a entré ni 'D', ni 'd', ni 'R', ni 'r')
        {
            DisplayError(); // On affiche une erreur
            return 1; // On retourne 1, donc une erreur aux yeux de C.
        }
    }
    printf("The sphere's volume is %.2f m3", Volume); // On affiche le volume de la sphère

    return 0; // On retourne 0, le programme est fini, tout s'est bien passé.
}

void DisplayRadius()
{
    printf("\nEnter the radius : "); // On demande d'entrer le rayon
    scanf("%f", &Radius);            // On place l'entrée dans Radius
}

void DisplayDiameter()
{
    printf("\nEnter the diameter : "); // On demande d'entrer le diamètre
    scanf("%f", &Diameter);            // On place l'entrée dans Radius
}

void DisplayError()
{
    printf("\nError : Please type R or D."); // On affiche une erreur
}

/* J'attire l'attention sur le fait que les fonctions ci-dessus ne retourne pas de valeur, mais modifient directement
 * les variables globales du code source.
 */